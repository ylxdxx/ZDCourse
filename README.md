#  西藏大学课程论文模板

#### 介绍
西藏大学课程论文 LaTeX 模板

依照藏大研究生毕业论文LaTeX 模板得来，改了封面

## 说明

模板地址：https://gitee.com/ylxdxx/ZDCourse

在使用过程中有什么问题，可在上面仓库的问题区「issues」提出

#### 测试环境

- 系统：Debian 11
- 发行版：TexLive 2022
- 编译器：XeLaTeX
- 编辑器：TeXStudio

#### 其它相关

- 西藏大学硕士论文模板：https://gitee.com/ylxdxx/ZD-SS-template
- 西藏大学本科论文模板：https://gitee.com/ylxdxx/ZD-XS-template

